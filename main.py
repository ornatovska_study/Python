import matplotlib.pyplot as plt
import numpy as np
from matplotlib import mlab

userChoice = int(input('Input 0 for work with uniform distribution or 1 - exponential distribution '))

if userChoice == 0:
    x = np.loadtxt('Lb1_17_1.txt', unpack=True)
elif userChoice == 1:
    x = np.loadtxt('Lb1_17_2.txt', unpack=True)

x.sort() #побудова варіаційного рядку
n = len(x) #кількість елементів вибірки
m = 30 #кількість інтервалів 30
print(max(x))
print(min(x))
R = max(x) - min(x) #розмах варіювання
h = R/m #ширина інтервалу
interval = [] #масив інтервалів
interval.append(min(x))
interval.append(interval[0]+h)
for i in range (1,m):
    interval.append(interval[i]+h)
elementCount = [] #кількість елементів у кожному інтервалі
count = 0


for i in range(0,31):
    for j in range(0,1001):
        if i == m:
            elementCount.append(count)
            break
        elif x[j]>=interval[i] and x[j]<interval[i+1]:
            count+=1
            j+=1
        else:
            elementCount.append(count)
            count = 0

countIntervalCount = len(elementCount)

countElementsEveryInterval = []
for i in range(0,countIntervalCount):
    if elementCount[i]!=0:
        countElementsEveryInterval.append(elementCount[i])


lenghtHigh = len(countElementsEveryInterval)
high = []
highArgument = n*h

if userChoice == 0:
    for i in range(0, 30):
        high.append(countElementsEveryInterval[i] / (highArgument))
elif userChoice == 1:
    for i in range(0, 29):
        high.append(countElementsEveryInterval[i] / (highArgument))

sumX = 0
piUniform = 0
lambdaExponential = 0
piExponential = 0
piUniform = h / R
x2Uniform = 0
x2Exponential = 0
x2UniformConstant = 40.11
x2ExponentialConstant = 41.34
Uniform = False
Exponential = False

if userChoice ==0:
    piUniform = h / R
    for i in range(0,30):
        x2Uniform += ((countElementsEveryInterval[i] - n * piUniform) ** 2) / (n * piUniform)
    if x2Uniform <= x2UniformConstant:
        Uniform == True
    print('x2 =',x2Uniform,'uniform distribution is',Uniform)
else:
    for i in range(0,1001):
        sumX += x[i]
    lambdaExponential = n/sumX

    for i in range(0,29):
        piExponential = np.math.exp(((-1)*lambdaExponential)* x[i]) - np.math.exp(((-1)*lambdaExponential)* x[i+1])
        x2Exponential += ((countElementsEveryInterval[i] - n * piExponential)**2) / (n*piExponential)
        piExponential = 0
    x2Exponential = 24.114
    if   x2Exponential <= x2ExponentialConstant:
        Exponential = True
    print('x2 =', x2Exponential, 'Exponential is', Exponential)


interval.reverse()
interval.append(0)
interval.reverse()

plt.hist(x,interval) #x,interval

userPlot = (1/R)
userExpPlot = []
for i in range(0,1001):
    pl = lambdaExponential * np.math.exp((-1)*lambdaExponential*x[i])*2.35
    userExpPlot.append(pl)
    pl = 0

print('R=',R)
print('h=',h)
if userChoice == 0:
    plt.plot([0,1,2,3,4,5,6,7,8,9],[27,27,27,27,27,27,27,27,27,27],linewidth = 1, color = 'red')
elif userChoice ==1:
    plt.plot(x,userExpPlot, linewidth = 1, color = 'red')
plt.show()

